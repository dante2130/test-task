## Test Task
***
This task contains four mini tasks:

* **.NET**:
Console application that reads text file ?data.txt? and counts number of each unique word in it. Words should be compared using current culture, case-insensitive.

* **SQL**:
Schema for database that will store logistic data for small machine parts shop. And scripts with queries for this schema and generating demo data.

* **MVC**:
Small MVC Web App with list of Persons, which allows to add, remove and edit them.

* **PowerShell**:
Three small function, which operates with filesystem.