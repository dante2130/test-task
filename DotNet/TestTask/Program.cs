﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TestTask.Utils;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> words = WordsOccurInFileUtil.GetWords($@"{Environment.CurrentDirectory}\Resources\data.txt");
            if (words.Any())
            {
                WordsOccurInFileUtil.WriteWordOccurences(
                    $@"{Environment.CurrentDirectory}\Resources\results.txt", WordsOccurInFileUtil.FormatOutput(words));
            }
        }
    }
}
