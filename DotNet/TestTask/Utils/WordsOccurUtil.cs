﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace TestTask.Utils
{
    /// <summary>
    /// Find count of word occurences in text file
    /// </summary>
    public class WordsOccurInFileUtil
    {
        public static Dictionary<string, int> GetWords(string inputFilePath)
        {
            string resultLine = string.Empty;
            Dictionary<string, int> wordsOccur = new Dictionary<string, int>(StringComparer.CurrentCultureIgnoreCase);

            try
            {
                using (FileStream fs = new FileStream(inputFilePath, FileMode.Open))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        const int byteCount = 102400;

                        char[] buf = new char[byteCount];
                        int count = 0;

                        while (count < fs.Length)
                        {
                            int bytesRead = sr.Read(buf, 0, byteCount);
                            string resString;
                            IEnumerable<char> resultBuf = buf.Take(bytesRead);

                            if (buf[buf.Length - 1] != ' ')
                            {
                                string str = new string(buf);
                                char[] bufPartial = resultBuf.Take(str.LastIndexOf(" ", StringComparison.CurrentCultureIgnoreCase))
                                    .ToArray();
                                bytesRead = bufPartial.Length;
                                resString = new string(bufPartial);
                            }
                            else
                            {
                                resString = new string(buf);
                            }

                            resString = Regex.Replace(resString, "[^a-zA-Z0-9 ]+", string.Empty, RegexOptions.Compiled);
                            string[] wordsBuffer = resString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string word in wordsBuffer)
                            {
                                if (wordsOccur.ContainsKey(word))
                                {
                                    wordsOccur[word] = wordsOccur[word] + 1;
                                }
                                else
                                {
                                    wordsOccur.Add(word, 1);
                                }
                            }

                            count += bytesRead;
                            fs.Position = count;
                            sr.DiscardBufferedData();
                        }
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
            return wordsOccur;
        }

        /// <summary>
        /// Writing finded word occurences result to file
        /// </summary>
        /// <param name="outFilePath">Output text file path</param>
        /// <param name="content">Content what will be writed to file</param>
        public static void WriteWordOccurences(string outFilePath, string content)
        {
            byte[] buf = Encoding.Default.GetBytes(content);
            FileUtil.WriteFile(outFilePath, buf);
        }

        /// <summary>
        /// Method for sort and formatting finded word occurences
        /// </summary>
        /// <param name="words"></param>
        /// <returns></returns>
        public static string FormatOutput(Dictionary<string, int> words)
        {
            return string.Join("\n", words.OrderByDescending(word => word.Value)
                .ThenBy(word => word.Key)
                .Select(word => $"{word.Key} {word.Value}")).ToLower(CultureInfo.CurrentCulture);
        }
    }
}
