﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask.Utils
{
    class FileUtil
    {
        /// <summary>
        /// Write file to filesystem
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <param name="content">Content what will be writed to file</param>
        /// <param name="offset">Byte offset for writing content</param>
        /// <param name="fileMode">File mode</param>
        public static void WriteFile(string filePath, byte[] content, int offset = 0, FileMode fileMode = FileMode.Create)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, fileMode))
                {
                    fs.Write(content, offset, content.Length);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
