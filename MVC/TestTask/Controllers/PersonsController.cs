﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestTask.Data;
using TestTask.Models;

namespace TestTask.Controllers
{
    public class PersonsController : Controller
    {
        private PersonContext db = new PersonContext();

        public ActionResult Index()
        {
            var viewModel = new PersonIndexModel {Persons = db.Persons.Select(pers => pers)};
            return View(viewModel);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Name,BirthDate,Description")]PersonModel person)
        {
            if (ModelState.IsValid)
            {
                db.Persons.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }
        
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonModel person = db.Persons
                .Single(i => i.ID == id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        [Authorize]
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditRecord(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var personToUpdate = db.Persons
                .Single(i => i.ID == id);

            if (TryUpdateModel(personToUpdate, string.Empty,
                new[] { "Name", "BirthDate", "Description" }))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            return View(personToUpdate);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateDescription(
            int? id,
            [Bind(Prefix = "Description")]string description)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var personToUpdate = db.Persons
                .Single(i => i.ID == id);

            if (TryUpdateModel(personToUpdate, "",
                new[] { "Description" }))
            {
                try
                {
                    db.SaveChanges();
                    return Json(personToUpdate);
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }
            return Content("Failed to update person description");
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PersonModel person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PersonModel person = db.Persons
                .Single(i => i.ID == id);
            
            db.Persons.Remove(person);

            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}