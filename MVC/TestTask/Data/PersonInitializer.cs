﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TestTask.Models;

namespace TestTask.Data
{
    public class PersonInitializer : DropCreateDatabaseIfModelChanges<PersonContext>
    {
        protected override void Seed(PersonContext context)
        {
            var persons = new List<PersonModel>
            {
                new PersonModel { Name = "Emma", BirthDate = DateTime.Parse("2000-01-01"), Description = "Sample Description" },
                new PersonModel { Name = "Liam", BirthDate = DateTime.Parse("2001-01-01"), Description = "Sample Description" },
                new PersonModel { Name = "Jackson", BirthDate = DateTime.Parse("2002-01-01"), Description = "Sample Description" },
                new PersonModel { Name = "William", BirthDate = DateTime.Parse("2003-01-01"), Description = "Sample Description" },
                new PersonModel { Name = "Chloe", BirthDate = DateTime.Parse("2004-01-01"), Description = "Sample Description" },
                new PersonModel { Name = "Jacob", BirthDate = DateTime.Parse("2000-02-01"), Description = "Sample Description" },
                new PersonModel { Name = "Abigail", BirthDate = DateTime.Parse("2000-03-01"), Description = "Sample Description" },
                new PersonModel { Name = "Harper", BirthDate = DateTime.Parse("2000-04-01"), Description = "Sample Description" },
                new PersonModel { Name = "Alexander", BirthDate = DateTime.Parse("2000-05-01"), Description = "Sample Description" },
                new PersonModel { Name = "Matthew", BirthDate = DateTime.Parse("2000-06-01"), Description = "Sample Description" }
            };
            persons.ForEach(s => context.Persons.Add(s));
            context.SaveChanges();
        }
    }
}