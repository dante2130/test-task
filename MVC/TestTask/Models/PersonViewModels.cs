﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestTask.Models;

namespace TestTask.Models
{
    public class PersonIndexModel
    {
        public IEnumerable<PersonModel> Persons { get; set; }
    }
}