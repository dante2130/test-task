USE [TestTask];
GO

-- Get list of suppliers names that made at least one delivery to
-- region with name 'Minnesota'
BEGIN
	SELECT
		[sup].[Name] AS [Supplier name]
	FROM [Suppliers] [sup]
	JOIN [Deliveries] [del] ON [del].[SupplierID] = [sup].[ID]
	JOIN [Regions] [reg] ON [reg].[ID] = [del].[RegionID]
	WHERE
		[reg].[Name] = 'Minnesota'
	GROUP BY [sup].[Name]
END;

-- Get summary value for all parts supplied by supplier 'Ford'
-- during January 2000
BEGIN
	DECLARE @FromDate DATE = CAST('2000-01-01' AS DATETIME);
	DECLARE @ToDate DATE = DATEADD(DAY, -1, CAST('2000-02-01' AS DATETIME));

	SELECT
		SUM([ditem].[Price] * [ditem].[Count]) AS [Summary Parts]
	FROM [DeliveryItems] [ditem]
	JOIN [Deliveries] [del] ON [del].[ID] = [ditem].[DeliveryID]
	JOIN [Suppliers] [sup] ON [sup].[ID] = [del].[SupplierID]
	WHERE [sup].[Name] = 'Ford' AND [del].[Date] BETWEEN @FromDate AND @ToDate
END;

-- Get number of deliveries with total cost more than
-- 1000 USD (total cost is sum of price times number of parts
-- by all lines for this delivery), grouped by supplier (result table 
-- should have columns 'Supplier name' and 'Delivery number')
BEGIN
	DECLARE @TotalCost INT = $1000;

	SELECT
		[sup].[Name] AS [Supplier name],
		COUNT([del].[ID]) AS [Delivery number]
	FROM
		[Deliveries] [del]
	JOIN [DeliveryItems] [ditem] ON [ditem].[DeliveryID] = [del].[ID]
	JOIN [Suppliers] [sup] ON [sup].[ID] = [del].[SupplierID]
	WHERE ([ditem].[Count] * [ditem].[Price]) > @TotalCost
	GROUP BY [sup].[Name]
END;

-- Get all supplier names who had not made any deliveries to regions
-- where supplier 'Ford' has any delivery
BEGIN
	DECLARE @Supplier NVARCHAR(255) = 'Ford';

	SELECT
		[sup].[Name] AS [Supplier name]
	FROM
		[Suppliers] [sup]
	JOIN [Deliveries] [del] ON [del].[SupplierID] = [sup].[ID]
	JOIN [Regions] [reg] ON [reg].[ID] = [del].[RegionID]
	WHERE 
		[reg].[ID] NOT IN
		(SELECT
			[reg].[ID]
		FROM
			[Suppliers] [sup]
		JOIN [Deliveries] [del] ON [del].[SupplierID] = [sup].[ID]
		JOIN [Regions] [reg] ON [reg].[ID] = [del].[RegionID]
		WHERE [sup].[Name] = @Supplier
		GROUP BY [reg].[ID])
	GROUP BY [sup].[Name]
END;