-- Remove Demo Data
USE [TestTask];
GO

DELETE FROM [DeliveryItems]
DELETE FROM [Deliveries]
DELETE FROM [Suppliers]
DELETE FROM [Regions]
DELETE FROM [MachineParts]
GO