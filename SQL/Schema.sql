﻿-- Creating Database
CREATE DATABASE [TestTask];
GO

ALTER DATABASE [TestTask] SET RECOVERY SIMPLE
GO

-- Creating Database User
CREATE LOGIN [TestTask] WITH PASSWORD = 'TestTask', CHECK_POLICY = OFF;
GO

USE [TestTask];
GO

EXEC sp_changedbowner 'TestTask';
GO

-- Creating Schema
CREATE TABLE [MachineParts](
	[ID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(255),
	[Created] DATETIME DEFAULT GETDATE() NOT NULL,
	[Modified] DATETIME
);
GO

CREATE TABLE [Regions](
	[ID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(255),
	[Created] DATETIME DEFAULT GETDATE() NOT NULL,
	[Modified] DATETIME
);
GO

CREATE TABLE [Suppliers](
	[ID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL PRIMARY KEY,
	[Name] NVARCHAR(255),
	[Created] DATETIME DEFAULT GETDATE() NOT NULL,
	[Modified] DATETIME
);
GO

CREATE TABLE [Deliveries](
	[ID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL PRIMARY KEY,
	[SupplierID] UNIQUEIDENTIFIER NOT NULL,
	[RegionID] UNIQUEIDENTIFIER NOT NULL,
	[Date] DATETIME NOT NULL,
	[Created] DATETIME DEFAULT GETDATE() NOT NULL,
	[Modified] DATETIME,
	CONSTRAINT [FK_Supplier] FOREIGN KEY ([SupplierID]) REFERENCES [Suppliers] ([ID]),
	CONSTRAINT [FK_Region] FOREIGN KEY ([RegionID]) REFERENCES [Regions] ([ID]),
);
GO

CREATE TABLE [DeliveryItems](
	[ID] UNIQUEIDENTIFIER DEFAULT NEWID() NOT NULL PRIMARY KEY,
	[DeliveryID] UNIQUEIDENTIFIER NOT NULL,
	[MachinePartID] UNIQUEIDENTIFIER NOT NULL,
	[Price] MONEY NOT NULL, 
	[Count] INT NOT NULL,
	[Created] DATETIME DEFAULT GETDATE() NOT NULL,
	[Modified] DATETIME
	CONSTRAINT [FK_Delivery] FOREIGN KEY ([DeliveryID]) REFERENCES [Deliveries] ([ID]),
	CONSTRAINT [FK_MachinePart] FOREIGN KEY ([MachinePartID]) REFERENCES [MachineParts] ([ID]),
);
GO

-- Creating Triggers
CREATE TRIGGER [TR_UpdateMachinePart]
ON [MachineParts]
AFTER UPDATE
AS
BEGIN
	UPDATE [MachineParts] SET [Modified] = GETDATE()
END
GO

CREATE TRIGGER [TR_UpdateRegion]
ON [Regions]
AFTER UPDATE
AS
BEGIN
	UPDATE [Regions] SET [Modified] = GETDATE()
END
GO

CREATE TRIGGER [TR_UpdateSupplier]
ON [Suppliers]
AFTER UPDATE
AS
BEGIN
	UPDATE [Suppliers] SET [Modified] = GETDATE()
END
GO

CREATE TRIGGER [TR_UpdateDelivery]
ON [Deliveries]
AFTER UPDATE
AS
BEGIN
	UPDATE [Deliveries] SET [Modified] = GETDATE()
END
GO

CREATE TRIGGER [TR_UpdateDeliveryItems]
ON [DeliveryItems]
AFTER UPDATE
AS
BEGIN
	UPDATE [DeliveryItems] SET [Modified] = GETDATE()
END
GO

-- Creating Indexes
CREATE INDEX [IX_MachPart_Name] ON [MachineParts] ([Name]);
CREATE INDEX [IX_Reg_Name] ON [Regions] ([Name]);
CREATE INDEX [IX_Suppl_Name] ON [Suppliers] ([Name]);
CREATE INDEX [IX_Deliv_Suppl] ON [Deliveries] ([SupplierID]);
CREATE INDEX [IX_Deliv_Region] ON [Deliveries] ([RegionID]);
CREATE INDEX [IX_Deliv_Date] ON [Deliveries] ([Date]);
CREATE INDEX [IX_DelivItem_Deliv] ON [DeliveryItems] ([DeliveryID]);
CREATE INDEX [IX_DelivItem_MachPart] ON [DeliveryItems] ([MachinePartID]);
CREATE INDEX [IX_DelivItem_Price] ON [DeliveryItems] ([Price]);
CREATE INDEX [IX_DelivItem_Count] ON [DeliveryItems] ([Count]);
GO

-- Rebuilding Indexes
ALTER INDEX [IX_MachPart_Name] ON [MachineParts] REBUILD;
ALTER INDEX [IX_Reg_Name] ON [Regions] REBUILD;
ALTER INDEX [IX_Suppl_Name] ON [Suppliers] REBUILD;
ALTER INDEX [IX_Deliv_Suppl] ON [Deliveries] REBUILD;
ALTER INDEX [IX_Deliv_Region] ON [Deliveries] REBUILD;
ALTER INDEX [IX_Deliv_Date] ON [Deliveries] REBUILD;
ALTER INDEX [IX_DelivItem_Deliv] ON [DeliveryItems] REBUILD;
ALTER INDEX [IX_DelivItem_MachPart] ON [DeliveryItems] REBUILD;
ALTER INDEX [IX_DelivItem_Price] ON [DeliveryItems] REBUILD;
ALTER INDEX [IX_DelivItem_Count] ON [DeliveryItems] REBUILD;
GO