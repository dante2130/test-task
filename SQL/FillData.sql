-- Insert Demo Data
USE [TestTask];

-- [MachineParts] Table
INSERT INTO [MachineParts] ([Name])
VALUES
	('Handle'),
	('Clip of Main spring'),
	('Sleeve'),
	('Main spring'),
	('Base plate'),
	('Pad holder'),
	('Pad lever'),
	('Locking lever'),
	('Frame'),
	('Front cover'),
	('Cam shaft'),
	('Cam spring'),
	('Plunger assembly'),
	('Swing'),
	('Main wheel shaft'),
	('Wheel (1st - 10th)'),
	('Index gear (Dup 1.)'),
	('Index gear (Trip 1.)'),
	('Index gear (Quadr.)'),
	('Gear role (set)'),
	('Movement indicator (set)'),
	('Movement indicator Plate'),
	('Knock'),
	('Slider'),
	('Shaft'),
	('Index'),
	('Index spring'),
	('Gear pole shaft'),
	('Selector shaft'),
	('Selector spring'),
	('Pad holder clip (set)'),
	('Clip'),
	('Case'),
	('Spring'),
	('Shaft'),
	('Pad (doz)'),
	('Base plate screw'),
	('Front cover screw'),
	('E-ring');
GO

-- [Regions] Table
INSERT INTO [Regions] ([Name])
VALUES
	('Alabama'),
	('Arizona'),
	('Arkansas'),
	('California'),
	('Colorado'),
	('Connecticut'),
	('Delaware'),
	('Florida'),
	('Georgia'),
	('Hawaii'),
	('Idaho'),
	('Illinois'),
	('Indiana'),
	('Iowa'),
	('Kansas'),
	('Kentucky'),
	('Louisiana'),
	('Maine'),
	('Maryland'),
	('Massachusetts'),
	('Michigan'),
	('Minnesota'),
	('Mississippi'),
	('Missouri'),
	('Montana'),
	('Nebraska'),
	('Nevada'),
	('New Hampshire'),
	('New Jersey'),
	('New Mexico'),
	('New York'),
	('North Carolina'),
	('North Dakota'),
	('Ohio'),
	('Oklahoma'),
	('Oregon'),
	('Pennsylvania'),
	('Rhode Island'),
	('South Carolina'),
	('South Dakota'),
	('Tennessee'),
	('Texas'),
	('Utah'),
	('Vermont'),
	('Virginia'),
	('Washington'),
	('West Virginia'),
	('Wisconsin'),
	('Wyoming'),
	('D.C. neighborhoods'),
	('American Samoa'),
	('Guam'),
	('Northern Mariana Islands'),
	('Puerto Rico'),
	('U.S. Virgin Islands');
GO

-- [Suppliers] Table
INSERT INTO [Suppliers] ([Name])
VALUES
	('Chrysler'),
	('Chalmers'),
	('DeSoto'),
	('Dodge'),
	('Eagle'),
	('Fargo'),
	('Imperial'),
	('Jeep'),
	('Maxwell'),
	('Plymouth'),
	('Ram'),
	('SRT'),
	('Valiant'),
	('Ford'),
	('Continental'),
	('Edsel'),
	('Lincoln'),
	('Mercury'),
	('General Motors'),
	('Buick'),
	('Marquette'),
	('Cadillac'),
	('LaSalle'),
	('Cartercar'),
	('Chevrolet'),
	('Geo'),
	('Elmore'),
	('Ewing'),
	('GMC'),
	('Hummer'),
	('Oakland'),
	('Pontiac'),
	('Oldsmobile'),
	('Viking'),
	('Rainier'),
	('Saturn'),
	('Scripps Booth'),
	('Sheridan'),
	('Welch'),
	('Welch-Detroit'),
	('Tesla'),
	('AM General, Vehicle Production Group'),
	('Anteros'),
	('Aurica'),
	('Berrien'),
	('BXR'),
	('Dragon'),
	('Elio Motors'),
	('E-Z-GO'),
	('Falcon'),
	('Faraday'),
	('Fisker'),
	('Formula1 Street'),
	('Googly'),
	('Hennessey'),
	('Ida'),
	('Karma'),
	('Local'),
	('Lucra'),
	('Lyons'),
	('Montenae'),
	('Mosler'),
	('Niama-Reisser'),
	('Next Autoworks'),
	('Panoz'),
	('Polaris'),
	('Racefab'),
	('RDC'),
	('Rossion'),
	('Rezvani'),
	('Shelby American'),
	('SSC'),
	('Star'),
	('Studebaker'),
	('Tanom'),
	('Vantage'),
	('Vector'),
	('United Motorcar Company'),
	('Zimmer');
GO

-- [Deliveries] and [DeliveryItems]
IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL
	DROP TABLE [#TempTable];
GO


CREATE TABLE [#TempTable] ([ID] UNIQUEIDENTIFIER, [Number] BIGINT, [Date] DATE, [RegionID] UNIQUEIDENTIFIER, [SupplierID] UNIQUEIDENTIFIER);
GO	

IF OBJECT_ID('tempdb..#TempTableItems') IS NOT NULL
	DROP TABLE [#TempTableItems];
GO

CREATE TABLE [#TempTableItems] ([DeliveryID] UNIQUEIDENTIFIER, [MachinePartID] UNIQUEIDENTIFIER, [Price] MONEY, [Count] INT);
GO

DECLARE @cnt INT = 0;

BEGIN
	DECLARE @machinePartCount INT = (SELECT COUNT(ID) FROM [MachineParts]);
	DECLARE @regionCount INT = (SELECT COUNT(ID) FROM [Regions]);
	DECLARE @supplierCount INT = (SELECT COUNT(ID) FROM [Suppliers]);

	DECLARE @priceMax INT = 10000;
	DECLARE @countMax INT = 1000;

	DECLARE @daysBetween INT;
	DECLARE @daysRand INT;

	DECLARE @fromDate DATE = '2000-01-01';
	DECLARE @toDate DATE = '2000-02-01';

	WHILE @cnt < 100
	BEGIN
		SET @daysBetween = DATEDIFF(DAY, @fromDate, @toDate)
		SET @daysRand = CAST(RAND() * 10000 AS INT) % @daysBetween

		DECLARE @id UNIQUEIDENTIFIER = NEWID();
		INSERT INTO [#TempTable] ([ID], [Date], [RegionID], [SupplierID])
		VALUES(
			@id,
			DATEADD(DAY, @daysRand, @fromDate),
			(SELECT [ID] FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY [Name]) AS [RowNum] FROM [Regions]) reg WHERE [RowNum] = FLOOR(RAND()*(@regionCount-1)+1)),
			(SELECT [ID] FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY [Name]) AS [RowNum] FROM [Suppliers]) supp WHERE [RowNum] = FLOOR(RAND()*(@supplierCount-1)+1))
		);

		DECLARE @cntItem INT = 0;
		WHILE @cntItem < 3
		BEGIN
			INSERT INTO [#TempTableItems] ([DeliveryID], [MachinePartID], [Price], [Count])
			VALUES(
				@id,
				(SELECT [ID] FROM (SELECT *, ROW_NUMBER() OVER (ORDER BY [Name]) AS [RowNum] FROM [MachineParts]) reg WHERE [RowNum] = FLOOR(RAND()*(@machinePartCount-1)+1)),
				FLOOR(RAND()*(@priceMax-1)+1),
				FLOOR(RAND()*(@countMax-1)+1)
			);
			SET @cntItem = @cntItem + 1;
		END;
		SET @cnt = @cnt + 1;
	END;
	
	INSERT INTO [Deliveries] ([ID], [Date], [RegionID], [SupplierID])
	SELECT [ID], [Date], [RegionID], [SupplierID] FROM [#TempTable];

	INSERT INTO [DeliveryItems] ([DeliveryID], [MachinePartID], [Price], [Count])
	SELECT [DeliveryID], [MachinePartID], [Price], [Count] FROM [#TempTableItems];
END;

IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL DROP TABLE [#TempTable]
IF OBJECT_ID('tempdb..#TempTableItems') IS NOT NULL DROP TABLE [#TempTableItems]
GO