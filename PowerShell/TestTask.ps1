﻿<#
	Function what give content preview (first 10 symbols) of text files
#>
function Get-Files-ContentPreview($Directory, $ExtensionMask)
{
    Try
    {
        $foundedItems = Get-ChildItem -Recurse -Force -ErrorAction Stop -Path $Directory -Include *.$ExtensionMask | 
        Sort-Object FullName | 
        ForEach-Object -Process {New-Object PSObject -Property @{Path = $_.FullName; ContentPreview = Get-Content $_ -TotalCount 1 -ErrorAction Stop}}

        if($foundedItems.Length -gt 0) {
            foreach($item in $foundedItems)
            {
                if($item.$ContentPreview -ne $Null -and $item.$ContentPreview -ge 10) 
                {
                    $item.$ContentPreview = $item.$ContentPreview.Substring(0, 10)
                }
            }
            $foundedItems | Format-Table -AutoSize -Property Path, ContentPreview
        }
        else
        {
            Write-Host "Files with *.$ExtensionMask mask not found"
        }
    }
    Catch [System.Management.Automation.ItemNotFoundException]
    {
        Write-Host "Directory not found"
    }
}

Get-Files-ContentPreview -Directory "$PSScriptRoot\ContentPreview" -ExtensionMask "txt"

<#
    Function what give tag names and it count from xml document
#>
function Get-Xml-TagsUsage($File)
{
    Try
    {
        $doc = Get-Content $File -Raw -ErrorAction Stop
        $reader = [System.Xml.XmlReader]::create([System.IO.StringReader]$doc)
        $result = @{}
        while ($reader.read())
        {
            if($reader.NodeType -eq [System.Xml.XmlNodeType]::Element)
            {
                If(!$result.ContainsKey($reader.Name)) { $result.Add($reader.Name, 1) }
                Else { $result[$reader.Name] = $result[$reader.Name]+ 1 }
            }
        }
        if($result.Count -ge 0)
        {
            $tableFormat = @{Label="Name"; Expression={$_.Key}},@{Label="Count"; Expression={$_.Value}}

            $result.GetEnumerator() | 
            Sort Value -Descending | 
            Format-Table -AutoSize $tableFormat
        }
    }
    Catch [System.Management.Automation.ItemNotFoundException]
    {
        Write-Host "File not found"
    }
    Catch [System.Xml.XmlException]
    {
        Write-Host "File is incorrectly"
    }
}

Get-Xml-TagsUsage -File "$PSScriptRoot\XmlTags\Test.xml"

function Get-SessionVerificationToken($User, $Password) {
    $LoginUrl = "http://localhost/TestTask/Account/Login"

    $loginResponse = Invoke-WebRequest $LoginUrl -SessionVariable ws -UseBasicParsing
    $anonymousToken  = ($loginResponse.InputFields | Where { $_.name -eq "__RequestVerificationToken" }).value
    
    $body = @{
        "__RequestVerificationToken" = $anonymousToken;
        "Name" = $User;
        "Password" = $Password;
    }

    $authenticatedResponse = Invoke-WebRequest $LoginUrl -Body $body -Method Post -WebSession $ws -UseBasicParsing
    $authenticatedToken = ($authenticatedResponse.InputFields | Where { $_.name -eq "__RequestVerificationToken" }).value

    return @{ Token = $authenticatedToken; WebSession = $ws }
}

function Set-PersonDescription($InputFile, $OutputFile, $Url, $User, $Password)
{
    try
    {
        $tokenResponse = Get-SessionVerificationToken -User $User -Password $Password

        $requestBody = @{
            "__RequestVerificationToken" = $tokenResponse.Token;
            "Description" = Get-Content $InputFile -Raw -ErrorAction Stop;
        }
        $response = Invoke-WebRequest $Url -Body $requestBody -Method Post -WebSession $tokenResponse.WebSession -UseBasicParsing
    
        Add-Content $OutputFile $response.Content -Force
    }
    Catch [System.Management.Automation.ItemNotFoundException]
    {
        Write-Host "File not found"
    }
}

Set-PersonDescription -InputFile "$PSScriptRoot\WebService\InputFile.txt" -OutputFile "$PSScriptRoot\WebService\OutputFile.txt" -Url "http://localhost/TestTask/Persons/UpdateDescription/1" -User "admin" -Password "admin"